## Synopsis
This directory contains the recipe to create a vagrant box running Confluent Schema Registry.

## Usage
The schema registry port (8081) is forwarded from Virtualbox host to the Virtualbox guest. So you can start using the [Confluent Schema Registry quickstart](http://docs.confluent.io/current/schema-registry/docs/intro.html#quickstart) as soon as the box is started: 
```sh
$ curl -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" \
    --data '{"schema": "{\"type\": \"string\"}"}' \
    http://localhost:8081/subjects/Kafka-key/versions
```

## Motivation
My colleagues wanted a simple way of testing Confluent Schema Registry, I'm just hoping to get a free coffee out of this deal.

## Installation

```sh
$ vagrant up
```

## Contributors
These recipes are maintained by Joris Visscher.

## License
Everything in this repository is provided under the MIT License