## Synopsis

This repo contains Vagrant recipes for running Confluent packages.

For more information about Vagrant, visit: [vagrantup.com](https://www.vagrantup.com/)

## Motivation

This project exists because I like simple setups to test real life scenarios. Each directory contains a Vagrant recipe for a specific purpose.

## Installation

git clone this repository and do a vagrant up in any of the directories. Look in each directory for a recipe specific README.

## Contributors

These recipes are maintained by Joris Visscher.

## License

Everything in this repository is provided under the MIT License