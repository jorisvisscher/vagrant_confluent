rpm --import http://packages.confluent.io/rpm/3.3/archive.key
cp /vagrant/confluent.repo /etc/yum.repos.d/confluent.repo
wget --quiet --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u144-b01/090f390dda5b47b9b721c7dfaa008135/jre-8u144-linux-x64.rpm -O /tmp/jre-linux-x64.rpm
yum clean all
yum -y -q install /tmp/jre-linux-x64.rpm

yum -y -q install confluent-kafka-2.11 confluent-schema-registry
nohup zookeeper-server-start /etc/kafka/zookeeper.properties > /tmp/zookeeper.out &
sleep 5
nohup kafka-server-start /etc/kafka/server.properties > /tmp/broker.out &
sleep 20
nohup schema-registry-start /etc/schema-registry/schema-registry.properties > /tmp/schema-registry.out &
